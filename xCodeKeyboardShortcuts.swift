//
//  xCodeKeyboardShortcuts.swift
//  
//
//  Created by Christopher J. Roura on 5/8/19.
//

import UIKit

Shortcuts

All keys black and each section will have colored outline on key
    
⌘ key           = .purple
Shift key       = .green
Alt/option key  = .blue
Control key     = .gray

General: green
⌘ ,             preferences
Fn ⌫            delete from right side of the cursor
⌘ S             save
⌘ Y             redo (only on some programs)
⌘ ⌃ F           full screen
⌘ ⌥ ⇧ V         paste keeping formatting (doesn’t work always dkw)
⌘ ⌃ ␣           show emojis
⌘ M             minimize window
⌘ Q             quit
⌃ →             go to the next desktop
⌃ ←             go to the previous desktop
⌘ ⇧ 3           screenshot whole screen
⌘ ⇧ 4           screenshot selection
⌘ ⇧ 4 space     screenshot windowed selection
⌘ ⇧ 5           shows screenshot options
⌘ ⇧ 6           screenshot Touch-Bar
⌘ ⌃ ⇧ 3         copy screenshots to clipboard
⌘ ⌃ ⇧ 4         copy screenshot to clipboard
⌘ ⌃ ⇧ 6         copy screenshot Touch-Bar to clipboard
⇧ ←             select letter to the cursor’s left
⇧ →             select letter to the cursor’s right
⌥ ←             move cursor word by word to the left
⌥ →             move cursor word by word to the right
⇧ ⌥ ←           select word to the cursor’s left
⇧ ⌥ →           select word to the cursor’s right
⌘ up            set cursor at the top of a row
⌘ ←             set cursor at the beginning of a row
⌘ →             set cursor at the end of a row
⌘ down          set cursor at the bottom of a row
⌘ ⇧ arrow       select row in arrow direction


Finder: blue color
⌘ N             new window
⌘ T             new tab
⌘ W             close tab
⌘ Z             undo
⌘ ⇧ Z           redo
⌘ A             select everything
⌘ F             search
⌘ D             duplicate
⌘ H             hide
⌘ ⌫             delete file
⌘ ⇧ ⌫           empty trash
⌘ ⌥ T           show / hide toolbox
⌘ /             show status bar


Xcode: purple color
⌘ N             new file
⌘ T             new tab
⌘ T [           cycle forward tab
⌘ T ]           cycle backward tab
⌘ W             close tab
⌘ ⌥ T           show / hide toolbox
⌘ F             search in current file
⌘ ⇧ F           search files in project
⌘ A             select everything
⌘ ⇧ S           duplicate file
⌘ /             comment
⌘ R             run
⌘ B             build
⌘ ⇧ K           clean build folder
⌘ ⌥ ⇧ K         deep clean build folder
⌘ ⌥ ←           collapse code block
⌘ ⌥ →           relapse code block
⌘ \             add / remove breakpoint
⌘ ⇧ 0           shows documentation
⌘ ⏎             show normal editor
⌘ ⇧ ⏎           show assistant editor
⌘ 1 - 9         left pane tabs switch
⌘ 0             hide navigator area
⌘ ⌥ 1 - 6       right pane tabs switch
⌘ ⌥ 0           hide inspector area
⌘ ⇧ Y           show / hide debug area
⌘ ⇧ 8           show Touch-Bar
⌘ ⌃ e           edit all in scope
⌘ ⌥ /           automatically add documentation
⌘ ⇧ o           quick find / open file
⌘ L             jumps to a code line
⌘ /             comment out a section or line
⌘ ⌥ click       jump to method definition
⌘ ⌥ ⇧ L         persist library
⌘ ⇧ m           media library

⌥ click         opens the clicked file in assistant editor
⌥ →             pass the cursor to the next word
⌥ ←             pass the cursor to the previous word

⌃ ␣             show current available completion
⌃ i             re-indents the highlighted function
⌃ 6             lists file contents
⌃ L             center cursor

Esc             show current available completion
